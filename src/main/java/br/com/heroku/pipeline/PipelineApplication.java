package br.com.heroku.pipeline;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class PipelineApplication {

	public static void main(String[] args) {
		SpringApplication.run(PipelineApplication.class, args);
	}

	@Value("${environment}")
	private String envDescription;

	@GetMapping(value = "/", produces = MediaType.TEXT_PLAIN_VALUE)
	public String showEnv() {
		return String.format("Hello! You are at %s environment\n", envDescription);
	}

	@GetMapping(value = "/{username}", produces = MediaType.TEXT_PLAIN_VALUE)
	public String userEnv(@PathVariable("username") String username) {
		return String.format("Hello %s! You are at %s environment\n", username, envDescription);
	}
}
